# remote_control

Remote_control is library for easy mapping python functions to remote buttons.

### Support for
  - pressing buttons
  - holding buttons
## Installation 
```sh
    $ pip install remote_control 
```

## Usage
Library can be used not only with arduino, it uses comports to listen for hex codes data.

#### Arduino sketch
```c
#include <IRremote.h>
#include <IRremoteInt.h>

#define irPin 11
IRrecv irrecv(irPin);
decode_results results;

void setup() {
   Serial.begin(9600);
   irrecv.enableIRIn();
}

void loop() {
   if (irrecv.decode(&results)) {
       Serial.println(results.value, HEX); 
       irrecv.resume();
   }
}
```

#### Python, eg.
```python
from volumeControl import *
from RemoteControl import RemoteControlWrapper
import sendkeys
import time

def volumeUp():
    volume.VolumeStepUp(None)
    volume.VolumeStepUp(None)

def volumeDown():
    volume.VolumeStepDown(None)
    volume.VolumeStepDown(None)

def playStopButton():
    sendkeys.key_down(179)
    time.sleep(0.1)
    sendkeys.key_up(179)

def prevButton():
    sendkeys.key_down(177)
    time.sleep(0.1)
    sendkeys.key_up(177)

def nextButton():
    sendkeys.key_down(176)
    time.sleep(0.1)
    sendkeys.key_up(176)

remote = RemoteControlWrapper(verbose=True)
remote.connectToFirstComportAvailable()

keys = {
    "up":"807F28D7",
    "down":"807F08F7",
    "play":"807FAA55",
    "prev":"807FE21D",
    "next":"807FA857"
}

remote.onButtonHold(keys["up"], volumeUp)
remote.onButtonHold(keys["down"], volumeDown)
remote.onButtonPress(keys["play"], playStopButton)
remote.onButtonPress(keys["prev"], prevButton)
remote.onButtonPress(keys["next"], nextButton)
```

### or

```python
from RemoteControl import RemoteControlWrapper

# Initialize wrapper:
remote = RemoteControlWrapper()
# Connect to comport number 9
remote.connect('com9')
# That's it, now you can do anything !
remote.onButtonPress("807FA857", os.system("shutdown"))
```

If you don't know your hex codes use:
```python
remote = RemoteControlWrapper(verbose=True)
remote.connect('com9')
```
and start pressing buttons on your remote.
### Version
0.1


License
----

MIT